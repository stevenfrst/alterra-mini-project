import {
    ApolloClient,
    InMemoryCache,
} from "@apollo/client";

import { WebSocketLink } from '@apollo/client/link/ws';

import { split, HttpLink } from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';

const httpLink = new HttpLink({
    uri: 'https://delicate-basilisk-69.hasura.app/v1/graphql',
    headers: {
        'x-hasura-admin-secret': '6KYf70o2HerWzWU45fAAbTQTFX12D0OgpLGHSmxHtsJfivLRK8T4EVdaJmK9A2IM'
    }
});

const wsLink = new WebSocketLink({
    uri: 'wss://delicate-basilisk-69.hasura.app/v1/graphql',
    options: {
        reconnect: true,
        connectionParams:{
            headers: {
                'x-hasura-admin-secret': '6KYf70o2HerWzWU45fAAbTQTFX12D0OgpLGHSmxHtsJfivLRK8T4EVdaJmK9A2IM'
            }
        }
    },

});

// The split function takes three parameters:
//
// * A function that's called for each operation to execute
// * The Link to use for an operation if the function returns a "truthy" value
// * The Link to use for an operation if the function returns a "falsy" value
const splitLink = split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return (
            definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
        );
    },
    wsLink,
    httpLink,
);




const client = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache(),
});

export default client
