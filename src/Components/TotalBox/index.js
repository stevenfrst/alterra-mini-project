import React, { useState } from "react"
import { useSelector, useDispatch} from "react-redux"
import { resetCart,changeTotal } from "../../store/action/Cart"
import styled from "styled-components"
import {Button} from "react-bootstrap";
import { useHistory } from 'react-router-dom';
import "./total.component.css"
import ButtonModalCheckout from "../../Pages/PrintPage"


const Box = styled.div`
  //position: fixed;
  bottom: 0;
  width: 22rem;
  height: 8rem;
  //box-shadow: 1px 1px 10px 1px #ccc;
  padding: 0.4rem;
  margin-left: -0.4rem;
  margin-bottom: 5rem;
`
const Total = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 0.4rem;
`
const Pay = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 0.4rem;
  input[type=number]{
    border: none;
    border-bottom: 1px solid #000;
    font-weight: bold;
    text-align: right;
    &:focus{
      outline: none;
    }
    &::-webkit-inner-spin-button,
    &::-webkit-outer-spin-button{
      -webkit-appearance: none;
    }
  }
`
const Change = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 0.6rem;
`
const BtnBox = styled.div`
  display: flex;
  justify-content: space-between;
`


const TotalBox = (props) => {
    const dispatch = useDispatch()

    const carts = useSelector(state => state.cart.carts)
    const total = () => {
        try {
            return carts.reduce((totalPrice, current) => totalPrice + current.harga, 0)
        } catch {
            return 0
        }
    }
    const [pay, setPay] = useState("")
    console.log("CART",pay-total())
    const [change, setChange] = useState(0)
    const handleChange = e => {
        setPay(e.target.value)
    }
    const calculateChange = () => {
        if(pay > total()){
            setChange(pay-total())
        }

        dispatch(changeTotal({
            total:total(),
            bayar:pay,
            kembalian:pay-total()
        }))
    }

    const reset = () => {
        dispatch(resetCart())
        setChange(0)
        setPay("")
        props.handlereset()
    }



    return(
        <Box>
            <Total>
                <h4>Total</h4>
                <p>{total()}</p>
            </Total>
            <Pay>
                <p>Jumlah Bayar</p>
                <br/>
                <input className="input-submit" type="number" value={pay} onChange={handleChange} />
            </Pay>
            <Change>
                <p>Kembalian</p>
                <br/>
                <input type="text" className="input-submit"  value={change}/>
            </Change>
            <BtnBox>
                <Button style={{backgroundColor:'#FB6C54',color:'white',border: '2px solid #E5E3E1',borderRadius:'10px'}} onClick={() => reset()}>Reset</Button>
                <Button style={{backgroundColor:'#FEBD2D',color:'black',border: '2px solid #E5E3E1',borderRadius:'10px'}} onClick={() => calculateChange()}>Kalkulasi</Button>
            </BtnBox>
            <ButtonModalCheckout/>
        </Box>
    )
}

export default TotalBox