import React from "react";
import "./categoryitem.component.css";
import {Button} from "react-bootstrap"

function CategoryItem(props) {
    const { makanan,click } = props;
    // console.log(typeof click)
    return (
        <>
            <div>
                <Button className="item-button" onClick={() => click(makanan)}>{makanan}</Button>
            </div>
        </>
    );
}

export default CategoryItem;
