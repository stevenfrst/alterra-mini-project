import React, {useState} from "react";
import styled from "styled-components"
import Counter from "../Counter";
import {useDispatch, useSelector} from "react-redux"
import { delCart,incCart,decCart } from "../../store/action/Cart"
import data from "bootstrap/js/src/dom/data";

const Cart = styled.div`
  display: flex;
  width: 95%;
  justify-content: space-between;
  align-items: center;
  height: 3rem;
  padding: 0 0.3rem;
  margin: 0.5rem auto;
  background: #fff;
  box-shadow: 1px 1px 10px 1px #ccc;
`
const CounterContainer = styled.div`
  display: flex;
  width: 30%;
  text-align: center;
`
const ItemName = styled.div`
  width: 40%;
  padding-left: 0.5rem;
`

const CounterTotal= styled.div`
  //margin: 0 0.1rem;
`
const Price = styled.div`
  width: 30%;
  text-align: center;
`



const CardItem = (props) => {
    const dispatch = useDispatch()
    const { name,datacart,trigger,index } = props;
    // const [count , setCount] = useState(1)
    const carts = useSelector(state => state.cart.carts)
    const increment = id => {
        // setCount(count + 1)
        dispatch(incCart(datacart))
    }

    const renderCount = () => {
        try {
            return carts[index].count
        } catch (error) {
            return ""
        }
    }

    const renderHarga = () => {
        try {
            return carts[index].harga
        } catch (error) {
            return 0
        }
    }

    const decrement = id => {
        // setCount(count - 1)
        if(carts[index].count === 1){
            trigger(id)
            dispatch(delCart(id))
            // setCount(carts[index].count)
        } else if (carts[index].count > 1) {
            dispatch(decCart(datacart))
        }
        console.log(id)
    }


    if (datacart === {}) {
        return null
    }
    return (
        <Cart>
            <ItemName>{name}</ItemName>
            <CounterContainer>
                <Counter inc={() => increment(datacart.id)}/>
                <CounterTotal>{renderCount()}</CounterTotal>
                <Counter dec={() => decrement(datacart.id)}/>
            </CounterContainer>
            <Price>
                {/*{item.price}*/}
                {renderHarga()}
            </Price>
        </Cart>

    )

}

export default CardItem