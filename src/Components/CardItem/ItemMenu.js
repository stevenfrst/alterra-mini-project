import React from "react";
import {Card,Button} from "react-bootstrap"
import {from} from "@apollo/client";


export default function ItemMenu(props) {
    const { namaMakanan, category, harga,toItem,dataRaw,itemValue,sizeItem,manage,fromHome,propsedit,propshapus} = props;
    console.log(sizeItem)
    let size = sizeItem
    return (
        <div>
            <Card style={{ width: '400px', height:'auto', margin:'0'}}>
                <Card.Body>
                    <Card.Title>{namaMakanan}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">{category}</Card.Subtitle>
                    <Card.Text>
                        {harga}
                        <p>

                        </p>
                        <Button hidden={fromHome} style={{marginLeft:'290px',backgroundColor:'#FEBD2D',color:'black',border: '2px solid #E5E3E1',borderRadius:'10px'}} onClick={() => toItem(dataRaw)}>{itemValue}</Button>
                        <Button hidden={manage} style={{marginLeft:'220px',backgroundColor:'#FEBD2D',color:'black',border: '2px solid #E5E3E1',borderRadius:'10px'}} onClick={() => propsedit({dataRaw})}>{itemValue}</Button>
                        <Button hidden={manage} style={{marginLeft:'15px',backgroundColor:'#FB6C54',color:'black',border: '2px solid #E5E3E1',borderRadius:'10px'}} onClick={() => propshapus(dataRaw.id)}>Hapus</Button>
                    </Card.Text>

                </Card.Body>
            </Card>
        </div>
    )
}