import React from "react";
import styled from "styled-components";


function Logo(props) {
    const { text5, vector } = props;

    return (
        <Logo1>
            <Text1>{text5}</Text1>
            <Vector src={vector} />
        </Logo1>
    );
}

const Logo1 = styled.div`
  height: 46px;
  display: flex;
  align-items: center;
  min-width: 150px;
`;

const Text1 = styled.h1`
  min-height: 46px;
  min-width: 122px;
  font-family: var(--font-family-shanti);
  font-weight: 400;
  color: var(--black);
  font-size: var(--font-size-xxxl);
  text-align: center;
  letter-spacing: 0;
`;

const Vector = styled.img`
  width: 24px;
  height: 34px;
  margin-left: 4px;
`;

export default Logo;
