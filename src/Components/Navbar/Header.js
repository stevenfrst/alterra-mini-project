import React from "react";
// import Logo from "../Logo/Logo";
// import { RobotoNormalBlack24px } from "../../StyleFont";
import {Nav,Navbar,Container} from "react-bootstrap"
import MakYekLogo from "../../img/Logo.svg"
import "./navbar.component.css"
import {Link} from "react-router-dom";

function Header(props) {
    const { aplikasi, manajemen, tentangAplikasi } = props;

    return (
        <>
        <Navbar id="Header">
            <Container>
                <Navbar.Brand href="#home"><img src={MakYekLogo} alt=""/></Navbar.Brand>
                <Nav className="ms-auto">
                    <Nav.Link href="/" className="item-header">{aplikasi}</Nav.Link>
                    <Nav.Link href="/manage" className="item-header">{manajemen}</Nav.Link>
                    <Nav.Link href="/about" className="item-header">{tentangAplikasi}</Nav.Link>
                </Nav>
            </Container>
        </Navbar>
        </>

    );
}



export default Header;
