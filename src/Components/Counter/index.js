import React from "react"
import styled from "styled-components"
import {Button} from "react-bootstrap"
import "./counter.component.css"



const Counter = ({ inc, dec }) => {
    if(inc){
        // return <CounterStyle onClick={inc}>+</CounterStyle>
        return <Button className="counter-css" onClick={inc}>+</Button>
    }else{
        // return <CounterStyle onClick={dec}>-</CounterStyle>
        return <Button className="counter-css" onClick={dec}>-</Button>
    }
}

export default Counter