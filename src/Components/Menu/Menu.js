import React from "react";
import "./menu.component.css"
import ItemMenu from "../CardItem/ItemMenu";



function Menu(props) {
    const { dataitem,click,isi,manageButton,homeButton,editData,hapusData } = props;

    return (
        <>
            <div className="kotak-menu">
                {dataitem?.frontend_pos_menu.map((item)=>
                    <ItemMenu propshapus={hapusData} propsedit={editData} fromHome={homeButton} manage={manageButton} namaMakanan={item.nama} category={item.category} harga={item.harga} toItem={click} dataRaw={item} itemValue={isi}/>
                )}
            </div>
        </>
    )
}

export default Menu;
