

const initialState = {
    carts: [],
    total: {
        total:0,
        bayar:0,
        // kembalian:0
    }
}

const cartReducer = (state=initialState,action) => {
    const { type, payload } = action
    switch(type) {
        default:
            return state
        case "ADD_CART":
            const itemInCart = state.carts.find(item => item.id === payload)
            console.log("ADD_CART:",state.carts,state)
            if(!itemInCart){
                return {
                    ...state,
                    carts: [...state.carts,payload]
                }
            } else {
                return state
            }
        case "DELETE":
            console.log("DELETE:",state.carts)
            return {
                ...state,
                carts: state.carts.filter(item => item.id !== payload)
            }
        case "INC_ITEM":
            const originalPriceINC = payload.harga
            console.log(originalPriceINC)
            const incCartINC = state.carts.map(item => {
                if (item.id === payload.id) {
                    return {
                        ...item,
                        harga: item.harga+originalPriceINC,
                        count:item.count+1
                    }
                } else {
                    return item
                }
            })
            return {
                ...state,
                carts: incCartINC
            }

        case "DEC_ITEM":
            const originalPriceDEC = payload.harga
            const incCartDEC = state.carts.map(item => {
                // console.log("",item.count)
                if (item.id === payload.id) {
                    return {
                        ...item,
                        harga: item.harga-originalPriceDEC,
                        count:item.count-1
                    }
                } else {
                    return item
                }
            })
            return {
                ...state,
                carts: incCartDEC
            }
        case "RESET":
            return {
                ...state,
                carts: []
            }

        case "GRABTOTAL":
            return {
                ...state,
                total: payload
            }
    }
}


export default cartReducer