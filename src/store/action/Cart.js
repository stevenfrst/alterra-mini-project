export const addCart = data => {
    return {
        type: 'ADD_CART',
        payload: data
    }
}

export const incCart = data => {
    return {
        type:"INC_ITEM",
        payload:data,
    }
}

export const decCart = data => {
    return {
        type:"DEC_ITEM",
        payload:data,
    }
}

export const delCart = id => {
    return{
        type:"DELETE",
        payload:id,
    }
}


export const resetCart = () => {
    return {
        type: "RESET",
    }
}


export const changeTotal = total => {
    return {
        type:"GRABTOTAL",
        payload:total
    }
}