import { css } from "styled-components";

export const RobotoNormalBlack18px = css`
  color: var(--black);
  font-family: var(--font-family-roboto);
  font-size: var(--font-size-xl2);
  font-weight: 400;
  font-style: normal;
`;

export const ShantiNormalBlack36px = css`
  color: var(--black);
  font-family: var(--font-family-shanti);
  font-size: var(--font-size-xxxl);
  font-weight: 400;
  font-style: normal;
`;

export const RobotoNormalBlack24px = css`
  color: var(--black);
  font-family: var(--font-family-roboto);
  font-size: var(--font-size-xxl);
  font-weight: 400;
  font-style: normal;
`;

export const RobotoNormalBlack13px = css`
  color: var(--black);
  font-family: var(--font-family-roboto);
  font-size: var(--font-size-m2);
  font-weight: 400;
  font-style: normal;
`;

export const Border1pxBlack = css`
  border: 1px solid var(--black);
`;
