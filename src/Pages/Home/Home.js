import React, {useState} from "react";
import "./home.component.css"
import Menu from "../../Components/Menu/Menu";
import CategoryItem from "../../Components/CategoryItem/CategoryItem";
import CardItem from "../../Components/CartItem";
import TotalBox from "../../Components/TotalBox";
import {gql,useSubscription} from "@apollo/client";
import {useDispatch, useSelector} from "react-redux"
import { addCart } from "../../store/action/Cart"
import Header from "../../Components/Navbar/Header";


const CategoryItems = ['Makanan','Minuman','Cemilan']


const filterViaCategory = gql`
    subscription MySubscription($where: frontend_pos_menu_bool_exp = {}) {
        frontend_pos_menu(where: $where) {
            category
            harga
            nama
        }
    }
`



export default function Home() {
    // const [category,setCategory] = useState({variables:{
    //     where:{category: {_eq: "Makanan"}}
    // }})

    const dispatch = useDispatch()
    const carts = useSelector(state => state.cart.carts)
    const [category,setCategory] = useState({})
    const [localCart,setLocalCart] = useState(carts)
    const [totalCart,setTotalCart] = useState(0)
    const {data,loading,error} = useSubscription(filterViaCategory,category)
    const handleGetCategory = makanan => {
        // console.log(makanan,typeof makanan)
        setCategory({variables:{
                where:{category: {_eq: makanan}}
            }})
        // console.log("hasura",data,loading,error)
    }

    // const getTotal = cart => {
    //     let counter = 0
    //     cart.map((item) => counter+=item.harga)
    //     return counter
    // }
    const handleResetCartLocal = () => {
        setLocalCart([])
    }

    const handleClickItem = toCart => {
        const newCart = {
            id:localCart.length+1,
            category:toCart.category,
            harga:toCart.harga,
            nama:toCart.nama,
            count:1
        }
        const updateCart = [
            ...localCart,newCart
        ]
        dispatch(addCart(newCart))
        setLocalCart(updateCart)
        console.log(localCart)
    }

    // const handleInc = () => {
    //     const IncCart
    // }
    const handleHapusItem = id => {
        console.log("TRIGER",id)
        const newCart = localCart.filter((item) => item.id !== id)
        setLocalCart(newCart)
    }

    // const addItemCount = id => {
    //     console.log("ADD",id)
    // }
    return (
        <>
            <Header
                aplikasi="Aplikasi"
                manajemen="Manajemen"
                tentangAplikasi="Tentang Aplikasi"
                logoProps={headerData.logoProps}
            />
        <div className="kotak-home">
            <div className="kotak-cart border-1px-bon-jour">
                {CategoryItems.map((item) =>
                    <CategoryItem makanan={item} click={handleGetCategory} />
                )}
            </div>
            <Menu manageButton={true} homeButton={false} dataitem={data} click={handleClickItem} isi="+"/>
            <div className="kotak-isi border-1px-bon-jour col">
                <div className="row-cols-8">
                    {localCart.map((item,index)=>
                        <div className="item-child">
                            {/*<p>{item.count}</p>*/}
                            <CardItem
                                name={item.nama}
                                price={item.harga}
                                datacart={item}
                                counter={item.count}
                                trigger={handleHapusItem}
                                index={index}
                            />

                        </div>
                    )}
                </div>
                <TotalBox handlereset={handleResetCartLocal}/>

                {/*<p>Total {getTotal(localCart)}</p>*/}
                {/*<CardItem/>*/}
                {/*<CardItem/>*/}
            </div>
        </div>
        </>
        )
}


const logoData = {
    text5: "MakYek",
    vector: "",
};

const headerData = {
    logoProps: logoData,
};

