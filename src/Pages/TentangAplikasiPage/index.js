import React from "react";
import "./AboutPage.component.css";
import gambarMakyek from "../../img/unnamed.jpeg"
import { useHistory } from "react-router-dom"

function TentangPage() {
    let history = useHistory();
    function handleClick() {
        history.push("/");
    }
    return (
        <div className="section">
            <div className="container-about">
                <div className="content-section">
                    <div className="title">
                        <h1>Tentang Aplikasi</h1>
                    </div>
                    <div className="content">
                        <h3>Aplikasi Point of Sales Makyek</h3>
                        <p>Nasi dan Ayam Goreng Makyek
                            Jl. Abimanyu V, Pindrikan Lor, Kec. Semarang Tengah, Kota Semarang,
                            Jawa Tengah 50131
                        </p>
                        <div className="button" onClick={handleClick}>
                            <a href>Kembali Ke Menu Utama</a>
                        </div>
                    </div>
                    <div className="social">
                        <a href><i className="fab fa-facebook-f" /></a>
                        <a href><i className="fab fa-twitter" /></a>
                        <a href><i className="fab fa-instagram" /></a>
                    </div>
                </div>
                <div className="image-section">
                    <img src={gambarMakyek} />
                </div>
            </div>
        </div>
    );
}


export default TentangPage;
