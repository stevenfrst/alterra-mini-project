import React, {useState} from "react";
import Header from "../../Components/Navbar/Header";
import CategoryItem from "../../Components/CategoryItem/CategoryItem";
import Menu from "../../Components/Menu/Menu";
import {gql, useSubscription,useMutation} from "@apollo/client";
import "./managePage.component.css"
import {Button} from "react-bootstrap";

const CategoryItems = ['Makanan','Minuman','Cemilan']

const filterViaCategory = gql`
    subscription MySubscription($where: frontend_pos_menu_bool_exp = {}) {
        frontend_pos_menu(where: $where) {
            id
            category
            harga
            nama
        }
    }
`

const deleteByID = gql`
    mutation MyMutation($id: Int_comparison_exp = {}) {
        delete_frontend_pos_menu(where: {id: $id}) {
            returning {
                id
                nama
                harga
                category
            }
        }
    }
`

const insertData = gql `
    mutation MyMutation($category: String, $harga: Int, $nama: String) {
        insert_frontend_pos_menu_one(object: {category: $category, harga: $harga, nama: $nama}) {
            category
            harga
            id
            nama
        }
    }
`

const updateHargaByID = gql`
    mutation MyMutation($harga: Int, $id: Int = 10) {
        update_frontend_pos_menu_by_pk(pk_columns: {id: $id}, _set: {harga: $harga}) {
            harga
            id
        }
    }

`

const baseMenu = {
    id: 0,
    nama: "",
    harga:"",
    category:"",
}

const ManajemenPage = () => {
    const [category,setCategory] = useState({})
    const {data} = useSubscription(filterViaCategory,category)
    const [deleteData, { data:dataDelete }] = useMutation(deleteByID);
    const [updateData, { data:dataUpdate }] = useMutation(updateHargaByID);
    const [insertDataALL, { data:dataInsert }] = useMutation(insertData);
    const [dataMenu,setDataMenu] = useState(baseMenu)
    const [status, setStatus] = useState("Submit")
    const handleGetCategory = makanan => {
        // console.log(makanan,typeof makanan)
        setCategory({variables:{
                where:{category: {_eq: makanan}}
            }})
        // console.log("hasura",data,loading,error)
    }
    const baseError = {
        nama: "",
    }
    const [errMSG, applyErrorMsg] = useState(baseError);

    const regexNama = /^[A-Za-z ]*$/
    const handleChange = e => {
        const name = e.target.name;
        const value = e.target.value;
        if (name === "nama") {
            if (!regexNama.test(value)) {
                applyErrorMsg({...errMSG, [name]: 'Nama Menu Harus Berupa Huruf'})
            } else {
                applyErrorMsg({...errMSG, [name]: ''})
            }
        }

        setDataMenu({...dataMenu,[name]:value})
    }

    const clickEdit = (data) => {
        setDataMenu({
            id: data.dataRaw.id,
            nama: data.dataRaw.nama,
            harga:data.dataRaw.harga,
            category:data.dataRaw.category,
        })
        setStatus("Edit")
        console.log("TRID",dataMenu)

    }

    const clickHapus = (id) => {
        console.log("TRIgger iD",id)
        deleteData({variables:{
            id:{_eq:id}
            }}
        )
    }

    const handleData = () => {
        if (status === "Edit") {
            console.log("edit")
            updateData({variables:{
                    id:dataMenu.id,
                    harga:dataMenu.harga
                }})
        } else {
            console.log("Submit")
            insertDataALL({variables:{
                    nama:dataMenu.nama,
                    harga:dataMenu.harga,
                    category:dataMenu.category
                }})
            setDataMenu(baseMenu)
        }
    }


    const handleReset = () => {
        setStatus("Submit")
        setDataMenu(baseMenu)
    }

    return (
        <>
            <Header
                aplikasi="Aplikasi"
                manajemen="Manajemen"
                tentangAplikasi="Tentang Aplikasi"
                logoProps={headerData.logoProps}
            />
            <div className="kotak-home">
                <div className="kotak-cart border-1px-bon-jour">
                    {/*<CategoryItem makanan="Favourite"/>*/}
                    {CategoryItems.map((item) =>
                        <CategoryItem makanan={item} click={handleGetCategory} />
                    )}
                </div>
                <Menu dataitem={data} isi="Edit" hapusData={clickHapus} editData={clickEdit} manageButton={false} homeButton={true}/>
                <div className="kotak-isi border-1px-bon-jour col">
                    <div className="row-cols-8">
                       <h3>Forms</h3>
                        <form>
                            <label>
                                Nama Menu:<br/>
                                <input
                                    required
                                    // className={styles.input}
                                    type="text"
                                    name="nama"
                                    value={dataMenu.nama}
                                    onChange={handleChange}
                                    className="input-submit"
                                />
                            </label>
                            <p>{errMSG.nama}</p>
                            <label>
                                Harga Menu:<br/>
                                <input
                                    required
                                    // className={styles.input}
                                    type="text"
                                    name="harga"
                                    value={dataMenu.harga}
                                    onChange={handleChange}
                                    className="input-submit"
                                />
                            </label>
                            <br/>
                            <label>
                                Category Menu: <br/>
                                <select
                                    required
                                    // type="text"
                                    name="category"
                                    value={dataMenu.category}
                                    onChange={handleChange}
                                    className="input-submit"
                                    style={{fontSize:'13px'}}
                                >
                                    <option disabled value="">Pilih Salah Satu Category</option>
                                    <option value="Makanan">Makanan</option>
                                    <option value="Minuman">Minuman</option>
                                    <option value="Cemilan">Cemilan</option>
                                </select>
                            </label>
                            <br/>
                            <br/>
                            <Button style={{backgroundColor:'#FEBD2D',color:'black',border: '2px solid #E5E3E1',borderRadius:'10px'}} onClick={handleData}>{status}</Button>
                            <Button style={{backgroundColor:'#FB6C54',color:'black',border: '2px solid #E5E3E1',borderRadius:'10px'}} onClick={handleReset}>Reset</Button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

const logoData = {
    text5: "MakYek",
    vector: "",
};

const headerData = {
    logoProps: logoData,
};



export default ManajemenPage