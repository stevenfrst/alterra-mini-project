import React, {useState} from "react";
import {useHistory} from "react-router-dom";
import { useSelector, useDispatch} from "react-redux"
import {Button, Modal} from "react-bootstrap"
import axios from "axios";

const PrintPage = () => {
    const toto = useSelector(state => state.cart.total)
    const carts = useSelector(state => state.cart.carts)
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    const handlePrint = () => {
        // console.log(printState)
        const obj = {
            'Tanggal Dibuat':date,
            'Total':toto.total,
            'Barang':JSON.stringify(carts)
        }
        console.log("log",obj)
        axios
            .post(
                'https://sheet.best/api/sheets/abd87f51-7431-43f8-8466-4883cea07db7',
                obj
            )
            .then((response) => {
                console.log(response);
            });
        alert("Success")
        setShow(false)
    }

    // <p>{toto.total}</p>
    // <p>{toto.bayar}</p>
    // <p>{toto.kembalian}</p>
    return (
        <>
            <Button style={{marginTop: "20px",backgroundColor:'#FEBD2D',color:'black',border: '2px solid #E5E3E1',borderRadius:'10px',width:'340px'}} onClick={handleShow}>Cetak</Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Checkout</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm">
                                <p><b>Nama Makanan</b></p>
                            </div>
                            <div className="col-sm">
                                <p><b>Jumlah</b></p>
                            </div>
                            <div className="col-sm">
                                <p><b>Harga</b></p>
                            </div>
                        </div>
                    </div>
                    {
                        carts.map((item) =>
                            <div className="container">
                                <div className="row">
                                    <div className="col-sm">
                                         <p>{item.nama}</p>
                                    </div>
                                    <div className="col-sm">
                                        <p>{item.count}</p>
                                    </div>
                                    <div className="col-sm">
                                        <p>{item.harga}</p>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                    <div className="container">
                        <div className="row">
                            <div className="col-sm">
                                 <h5>Total</h5>
                                 <p>{toto.total}</p>
                            </div>
                            <div className="col-sm">
                                <h5>Bayar </h5>
                                <p>{toto.bayar}</p>
                            </div>
                            <div className="col-sm">
                                <h5>Kembalian</h5>
                                <p>{toto.kembalian}</p>
                            </div>
                        </div>
                    </div>

                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Tutup
                    </Button>
                    <Button variant="primary" onClick={handlePrint}>
                        Print
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}


export default PrintPage