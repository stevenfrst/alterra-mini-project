import React from "react";
import {Route,Switch} from "react-router-dom"
import Home from "../Pages/Home/Home"
import About from "../Pages/TentangAplikasiPage"
import PrintPage from "../Pages/PrintPage"
import ManajemenPage from "../Pages/ManajemenPage/index"

function Routes() {
    return (
        <>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/about" component={About}/>
                <Route path="/checkout" component={PrintPage}/>
                <Route path="/manage" component={ManajemenPage}/>
            </Switch>
        </>
    );
}

export default Routes